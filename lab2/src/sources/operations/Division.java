package sources.operations;

import sources.calculator.StackCalculator;
import sources.exceptions.operations.OperationException;
import sources.exceptions.operations.ZeroDivisionException;

public class Division extends StackOperation {
    public Division() {
        super(0, 2);
    }

    @Override
    public void run(StackCalculator stackCalculator, String... args) throws OperationException {
        super.run(stackCalculator, args);
        Float up = stackCalculator.pop();
        Float down = stackCalculator.pop();

        if (up == 0)
            throw new ZeroDivisionException();

        stackCalculator.push(down/up);
    }
}
