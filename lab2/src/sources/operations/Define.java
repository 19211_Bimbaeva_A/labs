package sources.operations;

import sources.calculator.StackCalculator;
import sources.exceptions.operations.OperationException;
import sources.exceptions.operations.VariableAlreadyDefinedException;

public class Define extends StackOperation {
    public Define() {
        super(2, 0);
    }

    @Override
    public void run(StackCalculator stackCalculator, String... args) throws OperationException {
        super.run(stackCalculator, args);

        String name = args[0];
        Float value = Float.valueOf(args[1]);

        if (stackCalculator.isVariableDefined(name))
            throw new VariableAlreadyDefinedException(name, stackCalculator.getVariable(name), value);

        stackCalculator.defineVariable(name, value);
    }
}
