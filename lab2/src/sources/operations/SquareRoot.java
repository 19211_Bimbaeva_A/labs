package sources.operations;

import sources.calculator.StackCalculator;
import sources.exceptions.operations.NegativeSquareRootException;
import sources.exceptions.operations.OperationException;

public class SquareRoot extends StackOperation {
    public SquareRoot() {
        super(0, 1);
    }

    @Override
    public void run(StackCalculator stackCalculator, String... args) throws OperationException {
        super.run(stackCalculator, args);

        float value = stackCalculator.pop();
        if (value < 0)
            throw new NegativeSquareRootException(value);

        value = (float)Math.sqrt(value);
        stackCalculator.push(value);
    }
}
