package sources.operations;

import sources.calculator.StackCalculator;
import sources.exceptions.operations.OperationException;

public interface Operation {
    void run(StackCalculator stackCalculator, String... args) throws OperationException;
}
