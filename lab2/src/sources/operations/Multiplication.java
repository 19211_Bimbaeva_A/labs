package sources.operations;

import sources.calculator.StackCalculator;
import sources.exceptions.operations.OperationException;

public class Multiplication extends StackOperation {
    public Multiplication() {
        super(0, 2);
    }

    @Override
    public void run(StackCalculator stackCalculator, String... args) throws OperationException {
        super.run(stackCalculator, args);
        stackCalculator.push(stackCalculator.pop() * stackCalculator.pop());
    }
}
