package sources.operations;

import sources.calculator.StackCalculator;
import sources.exceptions.operations.OperationException;

public class Push extends StackOperation {
    public Push() {
        super(1, 0 );
    }

    @Override
    public void run(StackCalculator stackCalculator, String... args) throws OperationException {
        super.run(stackCalculator, args);

        Float value;

        try {
            value = Float.valueOf(args[0]);
        } catch (NumberFormatException e) {
            value = stackCalculator.getVariable(args[0]);
        }

        stackCalculator.push(value);
    }
}
