package sources.operations;

import sources.calculator.StackCalculator;
import sources.exceptions.operations.NotEnoughStackOperandsException;
import sources.exceptions.operations.OperationException;
import sources.exceptions.operations.WrongNumberOfRunMethodArguments;

public abstract class StackOperation implements Operation {
    private final int expectedRunMethodArguments;
    private final int expectedStackOperands;

    public StackOperation(int expectedRunMethodArguments, int expectedStackOperands) {
        this.expectedRunMethodArguments = expectedRunMethodArguments;
        this.expectedStackOperands = expectedStackOperands;
    }

    public void run(StackCalculator stackCalculator, String... args) throws OperationException {
        if (args.length != expectedRunMethodArguments)
            throw new WrongNumberOfRunMethodArguments(this.getClass(), expectedRunMethodArguments, args.length);

        if (stackCalculator.size() < expectedStackOperands)
            throw new NotEnoughStackOperandsException(this.getClass(), expectedStackOperands, stackCalculator.size());
    }
}
