package sources.operations;

import sources.calculator.StackCalculator;
import sources.exceptions.operations.OperationException;

public class Pop extends StackOperation {
    public Pop() {
        super(0, 1);
    }

    @Override
    public void run(StackCalculator stackCalculator, String... args) throws OperationException {
        super.run(stackCalculator, args);
        stackCalculator.pop();
    }
}
