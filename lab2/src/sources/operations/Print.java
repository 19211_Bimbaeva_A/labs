package sources.operations;

import sources.calculator.StackCalculator;
import sources.exceptions.operations.OperationException;

public class Print extends StackOperation {
    public Print() {
        super(0, 1);
    }

    @Override
    public void run(StackCalculator stackCalculator, String... args) throws OperationException {
        super.run(stackCalculator, args);

        float val = stackCalculator.pop();
        System.out.println(val);
        stackCalculator.push(val);
    }
}
