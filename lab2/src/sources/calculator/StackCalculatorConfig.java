package sources.calculator;

import sources.exceptions.config.BadContentException;
import sources.exceptions.config.ConfigIOException;
import sources.exceptions.config.MultipleDefinitionException;

import java.io.*;
import java.util.HashMap;

/**
 * Class contains connection between operation's name and operation's class name.
 */
class StackCalculatorConfig {
    private HashMap<String, String> operationTable;

    StackCalculatorConfig(String fileName) throws MultipleDefinitionException, ConfigIOException, BadContentException {
        read(fileName);
    }

    String getOperationClassName(String operationName) {
        return operationTable.get(operationName);
    }

    private void read(String fileName) throws BadContentException, MultipleDefinitionException, ConfigIOException {
        //TODO: implement file reading due to specification of task using Object.class.getResourceAsStream(fileName)
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            HashMap<String, String> newMap = new HashMap<String, String>();
            int row = 0;

            while (bufferedReader.ready()) {
                String lineContent = bufferedReader.readLine();
                String[] line = lineContent.split(" ");

                if (line.length != 2)
                    throw new BadContentException(row, lineContent);

                String operationName = line[0];
                String operationClass = line[1];

                if (newMap.containsKey(operationName))
                    throw new MultipleDefinitionException(row, lineContent, operationName);

                newMap.put(operationName, operationClass);

                row++;
            }

            bufferedReader.close();
            operationTable = newMap;
        } catch (IOException e) {
            throw new ConfigIOException(fileName);
        }
    }
}
