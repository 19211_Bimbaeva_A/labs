package sources.calculator;

import sources.exceptions.config.ConfigException;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.logging.*;

/**
 * Represents container for operands and operations results
 */

public class StackCalculator extends Stack<Float> {
    private Map<String, Float> constantPool;
    private Logger logger;

    public StackCalculator() {
        constantPool = new HashMap<>();

        // Logger initialising
        logger = Logger.getLogger(this.getClass().getName());
        logger.setLevel(Level.ALL);
        try {
            logger.addHandler(new FileHandler("logs.txt"));
        } catch (IOException e) {
            logger.addHandler(new ConsoleHandler());
        }
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////
    // Logging implementation

    @Override
    public Float push(Float item) {
        logger.log(Level.FINE, "push " + item);
        return super.push(item);
    }

    @Override
    public synchronized Float pop() {
        Float result = super.pop();
        logger.log(Level.FINE, "pop " + result);
        return result;
    }

    private void execute(Class<?> operationClass, List<String> args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Object operation = operationClass.getConstructor().newInstance();
        Method method = operationClass.getMethod("run", StackCalculator.class, String[].class);

        logger.log(Level.FINE, "Running operation: " + operationClass);
        method.invoke(operation, this, args.toArray(new String[]{}));
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////
    // Executing implementation

    /**
     * Running calculator with specific environment (operations, executing file name)
     * @param workingDirectory is directory with operations classes
     * @param configFileName purpose is clear
     * @param inputFileName file with commands
     */
    public void run(String workingDirectory, String configFileName, String inputFileName) {
        StackCalculatorConfig config;
        BufferedReader reader;

        String operationClassName;
        DynamicCompiler operationCompiler = new DynamicCompiler(workingDirectory);
        try {
            config = new StackCalculatorConfig(workingDirectory + "\\" + configFileName);
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputFileName)));


            // Reading file with commands and trying to interpret them one by one with information given by config
            while (reader.ready()) {
                List<String> line = Arrays.asList(reader.readLine().split(" "));
                line = new ArrayList<>(line);

                operationClassName = config.getOperationClassName(line.get(0));

                Class operationClass = operationCompiler.getClazz(operationClassName);
                // Executing operation
                line.remove(0);

                execute(operationClass, line);
            }

        } catch (IOException e) {
            e.printStackTrace();
            logger.log(Level.WARNING, e.getMessage());
        } catch (InstantiationException | NoSuchMethodException | IllegalAccessException e) {
            System.out.println("### Some error due to operation class name");
            e.printStackTrace();
            logger.log(Level.WARNING, e.getMessage());
        } catch (ConfigException e) {
            System.out.println("### Error due to config file reading");
            e.printStackTrace();
            logger.log(Level.WARNING, e.getMessage());
        } catch (InvocationTargetException e) {
            System.out.println("### Error due to operation executing");
            e.printStackTrace();
            logger.log(Level.WARNING, e.getMessage());
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    // Methods required to interact with constant pool

    public Float getVariable(String varName) {
        return constantPool.get(varName);
    }

    public void defineVariable(String variableName, float value) {
        constantPool.put(variableName, value);
    }

    public boolean isVariableDefined(String name) {
        return constantPool.containsKey(name);
    }

    public static void main(String[] argv) {
        if (argv.length != 3) {
            System.out.println("Enter working directory, file config name, file execution name");
            return;
        }

        StackCalculator stackCalculator = new StackCalculator();
        stackCalculator.run(argv[0], argv[1], argv[2]);
    }
}
