package sources.calculator;

import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

class DynamicCompiler extends ClassLoader{
    private HashMap<String, Class> compiledClasses;
    private String workingDirectory;
    DynamicCompiler(String workingDirectory) {
        compiledClasses = new HashMap<>();
        this.workingDirectory = workingDirectory;
    }

    /**
     * Checks if this class is already loaded or class file generated,
     * if not, loads it and compiles with SystemJavaCompiler.
     * @param operationClassName operation to compile
     * @throws IOException due to work with files
     */
    private void compile(String operationClassName) throws IOException {
        if (compiledClasses.keySet().contains(operationClassName))
            return;

        File operationClassFile = new File(workingDirectory + "\\" + operationClassName + ".class");
        if (!operationClassFile.exists())
            generateClassFile(operationClassName);

        byte[] classData = new FileInputStream(workingDirectory + "\\" + operationClassName + ".class").readAllBytes();
        compiledClasses.put(operationClassName, defineClass(null, classData, 0, classData.length));
    }

    private void generateClassFile(String operationClassName) {
        File operation = new File(workingDirectory  + "\\" + operationClassName + ".java");
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);

        Iterable<? extends JavaFileObject> compilationUnit
                = fileManager.getJavaFileObjectsFromFiles(Collections.singletonList(operation));

        ArrayList<String> optionList = new ArrayList<>();
        optionList.add("-classpath");
        optionList.add(System.getProperty("java.class.path") + File.pathSeparator + workingDirectory);

        JavaCompiler.CompilationTask task = compiler.getTask(
                null,
                fileManager,
                null,
                optionList,
                null,
                compilationUnit);
        task.call();
    }

    public Class getClazz(String name) throws IOException {
        compile(name);
        return compiledClasses.get(name);
    }
}
