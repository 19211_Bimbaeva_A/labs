package sources.exceptions.config;

public class BadContentException extends RowBoundedConfigException {
    public BadContentException(int row, String content) {
        super(row, "content isn't allowed: " + content);
    }
}
