package sources.exceptions.config;

public class ConfigException extends Throwable{
    public ConfigException(String message) {
        super(message);
    }
}
