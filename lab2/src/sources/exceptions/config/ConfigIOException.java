package sources.exceptions.config;

public class ConfigIOException extends ConfigException {
    public ConfigIOException(String fileName) {
        super("Can't read config file " + fileName);
    }
}
