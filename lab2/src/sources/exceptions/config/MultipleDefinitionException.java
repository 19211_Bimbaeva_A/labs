package sources.exceptions.config;


public class MultipleDefinitionException extends RowBoundedConfigException {
    public MultipleDefinitionException(int row, String lineContent, String operationName) {
        super(row, "in line " + lineContent + " redefinition of " + operationName);
    }
}
