package sources.exceptions.config;

public class RowBoundedConfigException extends ConfigException {
    public RowBoundedConfigException(int row, String message) {
        super("In row " + row + " " + message);
    }
}
