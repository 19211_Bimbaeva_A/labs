package sources.exceptions.operations;

public class WrongNumberOfRunMethodArguments extends OperationException {
    public WrongNumberOfRunMethodArguments(Class opClazz, int expected, int actual) {
        super(opClazz + " takes " + expected + " arguments, but given " + actual);
    }
}
