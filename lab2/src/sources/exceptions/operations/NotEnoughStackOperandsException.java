package sources.exceptions.operations;

public class NotEnoughStackOperandsException extends OperationException {
    public NotEnoughStackOperandsException(Class operation, int expected, int actual) {
        super("StackOperation " + operation + " requires " + expected + " arguments, but given " + actual);
    }
}
