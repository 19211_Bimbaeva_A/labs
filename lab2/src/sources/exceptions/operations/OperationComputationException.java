package sources.exceptions.operations;

public class OperationComputationException extends OperationException {
    public OperationComputationException(String what) {
        super("Computation exception: " + what);
    }
}
