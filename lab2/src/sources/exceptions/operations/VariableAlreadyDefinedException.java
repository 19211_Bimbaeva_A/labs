package sources.exceptions.operations;

public class VariableAlreadyDefinedException extends VariableDefinitionException {
    public VariableAlreadyDefinedException(String name, Float variable, Float value) {
        super("Variable " + name + " is already defined as " + variable + ", but found another value: " + value);
    }
}
