package sources.exceptions.operations;

public class NegativeSquareRootException extends OperationComputationException {
    public NegativeSquareRootException(Float value) {
        super("Can't compute square root of " + value);
    }
}
