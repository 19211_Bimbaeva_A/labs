package sources.exceptions.operations;

public class ZeroDivisionException extends OperationComputationException {
    public ZeroDivisionException() {
        super("Zero division computation");
    }
}
