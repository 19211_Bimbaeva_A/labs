package sources.exceptions.operations;

public class OperationException extends Throwable {
    public OperationException(String message) {
        super(message);
    }
}
