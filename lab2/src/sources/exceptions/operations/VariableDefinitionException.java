package sources.exceptions.operations;

public class VariableDefinitionException extends OperationException {
    VariableDefinitionException(String what) {
        super("Value definition exception: " + what);
    }
}
