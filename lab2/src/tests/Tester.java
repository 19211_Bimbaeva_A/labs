package tests;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import tests.operations.OperationsTest;

public class Tester {
    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(OperationsTest.class);

        if (result.getFailureCount() == 0)
            System.out.println("No tests failed");
        else
            System.out.println("Failed tests:");

        for (Failure f : result.getFailures()) {
            System.out.println(f.toString());
        }
    }
}
