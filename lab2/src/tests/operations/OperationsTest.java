package tests.operations;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import sources.calculator.StackCalculator;
import sources.exceptions.operations.OperationException;
import sources.exceptions.operations.ZeroDivisionException;
import sources.operations.*;

import static java.lang.Float.valueOf;
import static org.junit.Assert.*;

public class OperationsTest {

    @BeforeClass
    public static void before() {
        System.out.println("OperationsTest started");
    }

    @AfterClass
    public static void after() {
        System.out.println("OperationsTest completed");
    }

    @Test
    public void zeroDivision() {
        StackCalculator stackCalculator = new StackCalculator();
        stackCalculator.push(0f);
        stackCalculator.push(0f);

        try {
            new Division().run(stackCalculator);
        } catch (OperationException e) {
            assertEquals(e.getClass(), ZeroDivisionException.class);
        }

    }

    @Test
    public void subtraction() throws OperationException {
        StackCalculator stackCalculator = new StackCalculator();
        stackCalculator.push(3f);
        stackCalculator.push(2f);

        new Subtraction().run(stackCalculator);

        assertEquals(stackCalculator.size(), 1);
        assertEquals(stackCalculator.pop(), valueOf(-1f));
    }

    @Test
    public void additional() throws OperationException {
        StackCalculator stackCalculator = new StackCalculator();
        stackCalculator.push(1f);
        stackCalculator.push(2f);

        new Addition().run(stackCalculator);

        assertEquals(stackCalculator.size(), 1);
        assertEquals(stackCalculator.pop(), valueOf(3f));
    }

    @Test
    public void multiplication() throws OperationException {
        StackCalculator stackCalculator = new StackCalculator();
        stackCalculator.push(12f);
        stackCalculator.push(4f);

        new Multiplication().run(stackCalculator);

        assertEquals(stackCalculator.size(), 1);
        assertEquals(stackCalculator.pop(), valueOf(48f));
    }

    @Test
    public void division() throws OperationException {
        StackCalculator stackCalculator = new StackCalculator();
        stackCalculator.push(12f);
        stackCalculator.push(4f);

        new Division().run(stackCalculator);

        assertEquals(stackCalculator.size(), 1);
        assertEquals(stackCalculator.pop(), valueOf(3f));
    }

    @Test
    public void sqrt() throws OperationException {
        StackCalculator stackCalculator = new StackCalculator();
        stackCalculator.push(4f);

        new SquareRoot().run(stackCalculator);

        assertEquals(stackCalculator.size(), 1);
        assertEquals(stackCalculator.pop(), valueOf(2f));
    }

    @Test
    public void push() throws OperationException {
        StackCalculator stackCalculator = new StackCalculator();

        new Push().run(stackCalculator, "5f");

        assertEquals(stackCalculator.size(), 1);
        assertEquals(stackCalculator.pop(), valueOf(5f));
    }

    @Test
    public void pop() throws OperationException {
        StackCalculator stackCalculator = new StackCalculator();
        stackCalculator.push(5f);

        new Pop().run(stackCalculator);

        assertEquals(stackCalculator.size(), 0);
    }

    @Test
    public void define() throws OperationException {
        StackCalculator stackCalculator = new StackCalculator();

        new Define().run(stackCalculator, "a", "5f");

        assertTrue(stackCalculator.isVariableDefined("a"));
        assertEquals(stackCalculator.getVariable("a"), valueOf(5f));
    }
}
