package tests.calculator;

import org.junit.AfterClass;
import org.junit.BeforeClass;

public class CalculatorTest {
    @BeforeClass
    public static void before() {
        System.out.println("CalculatorTest started");
    }

    @AfterClass
    public static void after() {
        System.out.println("CalculatorTest completed");
    }
}
